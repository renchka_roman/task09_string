package com.epam.course.renchka.bigtask;

import com.epam.course.renchka.bigtask.controller.Controller;
import com.epam.course.renchka.bigtask.model.Domain;
import com.epam.course.renchka.bigtask.model.Text;
import com.epam.course.renchka.bigtask.view.MyView;
import java.io.IOException;

public class Application {

  public static void main(String[] args) throws IOException {
    Domain domain = new Domain();
    Text model = new Text(domain);
    Controller controller = new Controller(model);
    MyView view = new MyView(controller);
    view.start();
  }
}
