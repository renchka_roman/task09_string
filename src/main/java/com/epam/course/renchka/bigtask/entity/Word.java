package com.epam.course.renchka.bigtask.entity;

import com.epam.course.renchka.bigtask.model.Domain;

public class Word implements Comparable<Word> {

  private String contents;

  public Word(String contents) {
    this.contents = contents;
  }

  public String getContents() {
    return contents;
  }

  @Override
  public int compareTo(Word o) {
    return contents.compareTo(o.getContents());
  }

  public Character getFirstConsonant() {
    for (char c : contents.toCharArray()) {
      if (!Domain.vowels.contains(c)) {
        return c;
      }
    }
    return contents.charAt(0);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof Word)) {
      return false;
    }
    Word word = (Word) obj;
    return contents.equals(word.getContents());
  }

  @Override
  public int hashCode() {
    return contents.hashCode();
  }

  @Override
  public String toString() {
    return contents;
  }
}
