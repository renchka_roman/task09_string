package com.epam.course.renchka.bigtask.model;

import com.epam.course.renchka.bigtask.entity.Sentence;
import com.epam.course.renchka.bigtask.entity.Word;
import java.util.List;
import java.util.Map;

public class Text {

  private Domain domain;

  public Text(Domain domain) {
    this.domain = domain;
  }

  public boolean readFile(String path) {
    return domain.readFile(path);
  }

  public Map<Word, Integer> findLargestNumberOfSentencesThatHaveSameWords() {
    return domain.findLargestNumberOfSentencesThatHaveSameWords();
  }

  public List<Sentence> getSentencesInAscendingOrder() {
    return domain.getSentencesInAscendingOrder();
  }

  public Word getUniqueWordOfFirstSentence() {
    return domain.getUniqueWordOfFirstSentence();
  }

  public List<Word> getWordsOfQuestionSentences(int length) {
    return domain.getWordsOfQuestionSentences(length);
  }

  public List<String> replaceFirsWordWithVowelLetterToLongerWord() {
    return domain.replaceFirsWordWithVowelLetterToLongerWord();
  }

  public List<Sentence> sortWordsOfSentencesByFirstCharAndGetIt() {
    return domain.sortWordsOfSentencesByFirstCharAndGetIt();
  }

  public List<Word> sortWordsOfTextByVowelsPerCent() {
    return domain.sortWordsOfTextByVowelsPerCent();
  }

  public List<Word> sortWordsBeginsWithVowelsByFirstConsonants() {
    return domain.sortWordsBeginsWithVowelsByFirstConsonants();
  }

  public List<Word> sortWordsByIncreasingOfSomeChar(char c) {
    return domain.sortWordsByIncreasingOfSomeChar(c);
  }

  public List<Word> sortInputWordsByOccurrenceCountInTextDesc(
      List<Word> words) {
    return domain.sortInputWordsByOccurrenceCountInTextDesc(words);
  }

  public List<Sentence> deleteSubstringThatBeginsAndEndsWithSomeChars(
      char startSymbol, char endSymbol) {
    return domain
        .deleteSubstringThatBeginsAndEndsWithSomeChars(startSymbol, endSymbol);
  }

  public String deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(
      int length) {
    return domain
        .deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(length);
  }

  public List<Word> sortWordsByDecreasingOfSomeChar(char c) {
    return domain.sortWordsByDecreasingOfSomeChar(c);
  }

  public String changeEveryWordByDeleteNextOccurrenceOfFirstChar() {
    return domain.changeEveryWordByDeleteNextOccurrenceOfFirstChar();
  }

  public Sentence replaceWordsWithSomeLengthBySomeString(
      int length, String str) {
    return domain.replaceWordsWithSomeLengthBySomeString(length, str);
  }
}
