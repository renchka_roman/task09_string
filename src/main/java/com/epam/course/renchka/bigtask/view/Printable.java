package com.epam.course.renchka.bigtask.view;

@FunctionalInterface
public interface Printable {

  void print();
}
